package rytone.chroma.shard.backbone;

import java.util.concurrent.CompletableFuture;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.WebSocket;

public class Backbone extends Thread {
	private final Logger log;
	private final OkHttpClient httpClient;
	private final Request wsRequest;

	private BackboneState connectionState;
	private WebSocket wsConn;

	public final CompletableFuture<ShardData> dataFuture;

	public Backbone(String ip, String port) {
		this.log = LogManager.getLogger("Backbone");
		this.connectionState = BackboneState.DISCONNECTED;
		this.httpClient = new OkHttpClient();
		this.wsRequest = new Request.Builder().get().url("ws://" + ip + ":" + port).build();

		this.dataFuture = new CompletableFuture();
	}

	public void setConnectionState(BackboneState s) {
		this.log.trace("Updating backbone state to {}", s);
		this.connectionState = s;
	}

	public BackboneState getConnectionState() {
		return this.connectionState;
	}

	public void shutdown() {
		this.wsConn.close(1000, "Disconnect initiated by shard");
	}

	public void run() {
		this.setConnectionState(BackboneState.CONNECTING);
		this.wsConn = this.httpClient.newWebSocket(this.wsRequest, new BackboneListener(this));
		this.log.debug("Closing OkHttpClient executorService"); // TODO execute this only on shutdown?
		this.httpClient.dispatcher().executorService().shutdown();
	}
}
