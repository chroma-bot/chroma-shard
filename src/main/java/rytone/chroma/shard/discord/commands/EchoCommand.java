package rytone.chroma.shard.discord.commands;

import java.util.LinkedHashMap;

import rytone.chroma.shard.discord.command.Command;
import rytone.chroma.shard.discord.command.CommandInfo;
import rytone.chroma.shard.discord.command.ExecuteParams;
import rytone.chroma.shard.discord.command.parse.Argument;
import rytone.chroma.shard.discord.command.parse.FullStringArgument;

public class EchoCommand implements Command {
	@Override
	public CommandInfo getInfo() {
		return new CommandInfo(
			new String[] {"echo"},
			"commands.echo"
		);
	}

	@Override
	public LinkedHashMap<String, Argument> getArguments() {
		LinkedHashMap<String, Argument> args = new LinkedHashMap<>();
		args.put("text", new FullStringArgument(true));
		return args;
	}

	@Override
	public void execute(ExecuteParams params) {
		FullStringArgument arg = (FullStringArgument)params.arguments.get("text");
		params.message.getChannel().sendMessage(arg.getValue()).queue();
	}
}
