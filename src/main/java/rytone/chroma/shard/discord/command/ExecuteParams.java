package rytone.chroma.shard.discord.command;

import java.util.LinkedHashMap;

import org.apache.logging.log4j.Logger;

import net.dv8tion.jda.core.entities.Message;

import okhttp3.OkHttpClient;

import rytone.chroma.shard.discord.command.parse.Argument;
import rytone.chroma.shard.locale.LocaleProvider;

public class ExecuteParams {
	public final Logger log;
	public final Message message;
	public final LinkedHashMap<String, Argument> arguments;
	public final OkHttpClient http;
	public final LocaleProvider locale;

	public ExecuteParams(Logger l, Message m, LinkedHashMap<String, Argument> a, OkHttpClient h, LocaleProvider lp) {
		this.log = l;
		this.message = m;
		this.arguments = a;
		this.http = h;
		this.locale = lp;
	}
}
