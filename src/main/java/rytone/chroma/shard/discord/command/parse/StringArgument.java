package rytone.chroma.shard.discord.command.parse;

import java.util.ArrayList;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class StringArgument implements Argument<String> {
	private String parsed;
	private final Boolean req;

	public StringArgument(Boolean required) {
		this.req = required;
	}

	public String parse(String in) throws ParseException {
		if (in.isEmpty()) {
			if (!this.req) {
				this.parsed = "";
				return "";
			}
			throw new ParseException("Argument requires content!");
		}

		ArrayList<String> split = new ArrayList<>();
		Matcher m = Pattern.compile("([^\"]\\S*|\".+?\")\\s*").matcher(in); // TODO account for single quotes by not using regex?
		while (m.find()) {
			split.add(m.group(1));
		}

		parsed = split.get(0).replace("\"", "");
		return in.substring(split.get(0).length(), in.length());
	}

	public String getValue() {
		return parsed;
	}
}
