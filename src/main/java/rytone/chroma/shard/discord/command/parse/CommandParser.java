package rytone.chroma.shard.discord.command.parse;

import java.util.LinkedHashMap;
import java.util.Map;

public class CommandParser {
	private String in;
	
	public CommandParser(String input) {
		this.in = input;
	}

	public void stripPrefix(String prefix) throws PrefixUnmatchedException {
		if (!this.in.startsWith(prefix)) {
			throw new PrefixUnmatchedException("");
		}
		this.in = this.in.substring(prefix.length(), this.in.length());
	}

	public String stripCommand() throws InvalidCommandException {
		if (this.in.isEmpty()) {
			throw new InvalidCommandException("Command cannot be empty!");
		}
		String[] split = this.in.split("\\s+");
		this.in = this.in.substring(split[0].length(), this.in.length()).trim();
		return split[0];
	}

	public void parseArguments(LinkedHashMap<String, Argument> args) throws ParseException {
		for (Map.Entry<String, Argument> m : args.entrySet()) {
			try {
				this.in = m.getValue().parse(this.in.trim());
			} catch (ParseException e) {
				throw new ParseException("On argument " + m.getKey() + ": " + e.getLocalizedMessage());
			}
		}
	}
}
