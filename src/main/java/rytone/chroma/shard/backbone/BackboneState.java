package rytone.chroma.shard.backbone;

public enum BackboneState {
	CONNECTING, HANDSHAKING, CONNECTED, DISCONNECTED, RECONNECTING
}
