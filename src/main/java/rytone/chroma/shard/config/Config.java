package rytone.chroma.shard.config;

import com.moandjiezana.toml.Toml;

import java.io.File;

public class Config {
	public PathConfig paths;
	public ShardConfig shard;
	public BackboneConfig backbone;

	public static Config load(String path) {
		return new Toml().read(new File(path)).to(Config.class);
	}
}
