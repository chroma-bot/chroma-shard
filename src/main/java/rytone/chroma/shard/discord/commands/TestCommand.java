package rytone.chroma.shard.discord.commands;

import java.util.LinkedHashMap;

import rytone.chroma.shard.discord.command.Command;
import rytone.chroma.shard.discord.command.CommandInfo;
import rytone.chroma.shard.discord.command.ExecuteParams;
import rytone.chroma.shard.discord.command.parse.Argument;
import rytone.chroma.shard.discord.command.parse.StringArgument;
import rytone.chroma.shard.discord.command.parse.IntegerArgument;
import rytone.chroma.shard.discord.command.parse.FlagArgument;

public class TestCommand implements Command {
	@Override
	public CommandInfo getInfo() {
		return new CommandInfo(
			new String[] {"test", "wtf"},
			"commands.test"
		);
	}

	@Override
	public LinkedHashMap<String, Argument> getArguments() {
		LinkedHashMap<String, Argument> args = new LinkedHashMap<>();
		args.put("text", new StringArgument(true));
		args.put("int", new IntegerArgument());
		args.put("flags", new FlagArgument(new String[] {"ayy", "lmao"}));
		return args;
	}

	@Override
	public void execute(ExecuteParams params) {
		StringArgument text = (StringArgument)params.arguments.get("text");
		IntegerArgument num = (IntegerArgument)params.arguments.get("int");
		FlagArgument flags = (FlagArgument)params.arguments.get("flags");

		StringBuilder sb = new StringBuilder();
		sb.append("```\n");
		sb.append("Text: ").append(text.getValue()).append("\n");
		sb.append("Integer: ").append(num.getValue()).append("\n");
		sb.append("Matched flags:\n");
		flags.getValue().forEach((k,v) -> {
			sb.append("    ").append(k).append(": ").append(v).append("\n");
		});
		sb.append("```");

		params.message.getChannel().sendMessage(sb.toString()).queue();
	}
}
