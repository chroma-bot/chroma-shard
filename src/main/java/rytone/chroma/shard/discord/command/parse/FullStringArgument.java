package rytone.chroma.shard.discord.command.parse;

public class FullStringArgument implements Argument<String> {
	private String parsed;
	private final Boolean req;

	public FullStringArgument(Boolean required) {
		this.req = required;
	}
	
	public String parse(String in) throws ParseException {
		if (in.isEmpty() && this.req) {
			throw new ParseException("Argument requires content!");
		}
		parsed = in;
		return "";
	}

	public String getValue() {
		return parsed;
	}
}
