package rytone.chroma.shard.discord.commands.status;

import java.util.LinkedHashMap;

import rytone.chroma.shard.discord.command.Command;
import rytone.chroma.shard.discord.command.CommandInfo;
import rytone.chroma.shard.discord.command.ExecuteParams;
import rytone.chroma.shard.discord.command.ExecuteException;
import rytone.chroma.shard.discord.command.parse.Argument;

public class BackboneStatus implements Command {
	@Override
	public CommandInfo getInfo() {
		return new CommandInfo(
			new String[] {"backbone"},
			"commands.status.backbone"
		);
	}

	@Override
	public LinkedHashMap<String, Argument> getArguments() {
		LinkedHashMap<String, Argument> args = new LinkedHashMap<>();
		return args;
	}

	@Override
	public void execute(ExecuteParams params) throws ExecuteException {
		throw new ExecuteException("Not implemented.");
	}
}
