package rytone.chroma.shard.config;

public class PathConfig {
	public String assets;
	public String backbone;
	public String protobuf;
	public String shard;
	public String web_client;
	public String web_server;
	public String locale;
}
