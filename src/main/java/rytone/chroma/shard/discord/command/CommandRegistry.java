package rytone.chroma.shard.discord.command;

import java.util.HashMap;

public class CommandRegistry {
	private HashMap<String, Command> commands;
	private HashMap<String, Command> aliasedCommands;
	
	public CommandRegistry() {
		this.commands = new HashMap<>();
		this.aliasedCommands = new HashMap<>();
	}

	public void register(Command c) {
		this.commands.put(c.getInfo().aliases[0], c);
		for (String a : c.getInfo().aliases) {
			this.aliasedCommands.put(a, c);
		}
	}

	public Command get(String alias) {
		return this.commands.get(alias);
	}

	public Command getAliased(String alias) {
		return this.aliasedCommands.get(alias);
	}

	public HashMap<String, Command> getAll() {
		return this.commands;
	}

	public HashMap<String, Command> getAllAliased() {
		return this.aliasedCommands;
	}
}
