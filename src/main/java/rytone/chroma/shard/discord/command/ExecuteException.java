package rytone.chroma.shard.discord.command;

public class ExecuteException extends Exception {
	public ExecuteException(String msg) {
		super(msg);
	}
}
