package rytone.chroma.shard.discord.command.parse;

public class InvalidCommandException extends Exception {
	public InvalidCommandException(String msg) {
		super(msg);
	}
}
