package rytone.chroma.shard.discord.commands;

import java.io.IOException;
import java.util.LinkedHashMap;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.JsonNode;

import okhttp3.Request;
import okhttp3.Response;

import rytone.chroma.shard.discord.command.Command;
import rytone.chroma.shard.discord.command.CommandInfo;
import rytone.chroma.shard.discord.command.ExecuteException;
import rytone.chroma.shard.discord.command.ExecuteParams;
import rytone.chroma.shard.discord.command.parse.Argument;

public class CatCommand implements Command {
	private Request catReq;
	private ObjectMapper om;

	public CatCommand() {
		this.catReq = new Request.Builder().get().url("http://random.cat/meow").build();
		this.om = new ObjectMapper();
	}

	@Override
	public CommandInfo getInfo() {
		return new CommandInfo(
			new String[] {"cat", "kitty", "meow"},
			"commands.cat"
		);
	}

	@Override
	public LinkedHashMap<String, Argument> getArguments() {
		return new LinkedHashMap<String, Argument>();
	}

	@Override
	public void execute(ExecuteParams params) throws ExecuteException {
		try {
			Response r = params.http.newCall(this.catReq).execute();

			JsonNode obj = this.om.readTree(r.body().string());
			String cat = obj.path("file").asText();

			params.message.getChannel().sendMessage(cat).queue();
		} catch (IOException e) {
			throw new ExecuteException(e.getLocalizedMessage());
		}
	}
}
