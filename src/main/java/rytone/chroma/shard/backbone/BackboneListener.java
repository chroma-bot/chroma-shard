package rytone.chroma.shard.backbone;

import rytone.chroma.shard.backbone.util.MessageWrap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import okhttp3.WebSocket;
import okhttp3.Response;
import okhttp3.WebSocketListener;

import okio.ByteString;

import com.google.protobuf.InvalidProtocolBufferException;

import rytone.chroma.protobuf.Common.Message;
import rytone.chroma.protobuf.Common.Message.MessageType;
import rytone.chroma.protobuf.Handshake.HandshakeInit;
import rytone.chroma.protobuf.Handshake.HandshakeInit.ConnectionType;
import rytone.chroma.protobuf.Handshake.ShardHandshakeResponse;

public class BackboneListener extends WebSocketListener {
	private final Logger log;
	private final Backbone backbone;

	public BackboneListener(Backbone b) {
		super();
		this.backbone = b;
		this.log = LogManager.getLogger("BackboneListener");
	}

	@Override
	public void onOpen(WebSocket ws, Response r) {
		this.log.debug("Established connection.");
		this.backbone.setConnectionState(BackboneState.HANDSHAKING);
	}

	@Override 
	public void onClosing(WebSocket ws, int code, String reason) {
		this.log.debug("Backbone is disconnecting us! {} ({})", reason, code);
		this.log.trace("Sending disconnect response...");
		ws.close(1000, null);
	}

	@Override
	public void onClosed(WebSocket ws, int code, String reason) {
		this.log.warn("Disconnected from backbone! {} ({})", reason, code);
	}

	@Override
	public void onMessage(WebSocket ws, String data) {
		this.log.trace("Received text data over websocket: {}", data);
	}

	@Override
	public void onMessage(WebSocket ws, ByteString data) {
		this.log.trace("Received binary data (in base64) over websocket: {}", data.base64());
		this.log.trace("Assuming the data is a protobuf message and deserializing...");
		try {
			Message msg = Message.parseFrom(data.toByteArray());
			this.log.trace("Got message of type {}, passing on to handleMessage.", msg.getType());
			this.handleMessage(msg, ws);
		} catch (InvalidProtocolBufferException e) {
			this.log.warn("Protobuf appears to be invalid: {}", e.getLocalizedMessage());
		}
	}

	private void handleMessage(Message msg, WebSocket ws) {
		switch (this.backbone.getConnectionState()) {
			case HANDSHAKING:
				switch (msg.getType()) {
					case HELLO:
						this.log.trace("Sending shard handshake init...");
						
						HandshakeInit.Builder hsb = HandshakeInit.newBuilder();
						hsb.setType(ConnectionType.SHARD);
						hsb.setPid(0);
						hsb.setVersion("v1");

						byte[] init = hsb.build().toByteArray();
						byte[] ser_msg = MessageWrap.wrap(MessageType.HS_INIT, init);

						ws.send(ByteString.of(ser_msg));

						break;
					case SHARD_HS_RESP: 
						try {
							this.log.debug("Successfully connected to the backbone");

							ShardHandshakeResponse shr = ShardHandshakeResponse.parseFrom(msg.getPayload());
							ShardData dat = new ShardData(shr.getShardId(), shr.getShardCount());
							this.backbone.dataFuture.complete(dat);

							this.backbone.setConnectionState(BackboneState.CONNECTED);
						} catch (InvalidProtocolBufferException e) {
							this.log.error(e);
						}
						break;
					default:
						this.log.error("Received invalid message for our current state!");
						break;
				}
				break;
			case CONNECTED:
				switch (msg.getType()) {
					case HEARTBEAT:
						byte[] hbr = MessageWrap.wrap(MessageType.HEARTBEAT, new byte[0]);
						this.log.trace("Sending response heartbeat...");
						ws.send(ByteString.of(hbr));
						break;
					default:
						this.log.error("Received invalid message for our current state!");
				}
				break;
			default:
				this.log.error("Current state is invalid! (or not handled)");
				break;
		}
	}
}
