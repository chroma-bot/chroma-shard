package rytone.chroma.shard.locale;

import java.util.HashMap;
import java.util.stream.Stream;

import java.io.InputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.Path;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.JsonNode;

public class LocaleProvider {
	private final Logger log;

	private final ObjectMapper om;

	private final HashMap<String, JsonNode> locales;
	private final HashMap<String, String> abbrevTable;

	public LocaleProvider() {
		this.log = LogManager.getLogger("LocaleProvider");

		this.om = new ObjectMapper();

		this.locales = new HashMap<>();
		this.abbrevTable = new HashMap<>();
	}

	public LocaleProvider load(String loadPath) {
		this.log.debug("Loading locales from {}...", loadPath);
		try (Stream<Path> paths = Files.walk(Paths.get(loadPath))) {
			paths.filter(Files::isRegularFile)
				.filter(p -> p.toString().endsWith(".json"))
				.forEach(p -> {
					this.log.trace("Loading locale {}", p.toString());
					try {
						JsonNode json = this.om.readTree(Files.newInputStream(p));
						String name = json.path("name").asText().toLowerCase();
						String abbrev = json.path("abbrev").asText().toLowerCase();
						
						this.locales.put(name, json);
						this.abbrevTable.put(abbrev, name);
					} catch (IOException e) {
						this.log.warn("Failed to read locale {}", p.toString());
						e.printStackTrace();
					}
				});
		} catch (IOException e) {
			this.log.error("Failed to walk load path!");
			e.printStackTrace();
		}
		return this;
	}

	public String get(String locale, String path) throws LocaleException {
		if (!this.locales.containsKey(locale.toLowerCase())) {
			throw new LocaleException("No such locale " + locale);
		}
		JsonNode n = this.locales.get(locale);
		for (String seg : path.split("\\.")) {
			n = n.path(seg);
		}
		return n.asText();
	}

	public String getByAbbrev(String locale, String path) throws LocaleException {
		if (!this.abbrevTable.containsKey(locale.toLowerCase())) {
			throw new LocaleException("No such locale " + locale);
		}
		return this.get(this.abbrevTable.get(locale), path);
	}
}
