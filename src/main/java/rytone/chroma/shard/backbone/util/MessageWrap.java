package rytone.chroma.shard.backbone.util;

import com.google.protobuf.ByteString;

import rytone.chroma.protobuf.Common.Message;
import rytone.chroma.protobuf.Common.Message.MessageType;

public class MessageWrap {
	public static byte[] wrap(MessageType type, byte[] payload) {
		Message.Builder b = Message.newBuilder();
		b.setType(type);
		b.setPayload(ByteString.copyFrom(payload));
		return b.build().toByteArray();
	}
}
