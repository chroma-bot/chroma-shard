package rytone.chroma.shard.discord.command;

import java.util.LinkedHashMap;

import rytone.chroma.shard.discord.command.parse.Argument;

public interface Command {
	public LinkedHashMap<String, Argument> getArguments();
	public CommandInfo getInfo();

	public void execute(ExecuteParams params) throws ExecuteException;
}
