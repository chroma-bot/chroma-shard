package rytone.chroma.shard.locale;

public class LocaleException extends Exception {
	public LocaleException(String msg) {
		super(msg);
	}
}
