package rytone.chroma.shard.discord.command;

import java.util.LinkedHashMap;

import rytone.chroma.shard.discord.command.parse.Argument;
import rytone.chroma.shard.discord.command.parse.StringArgument;
import rytone.chroma.shard.discord.command.parse.FullStringArgument;
import rytone.chroma.shard.discord.command.parse.CommandParser;
import rytone.chroma.shard.discord.command.parse.ParseException;

public class MetaCommand implements Command {
	private final String localepath;
	private final String[] aliases;
	private final CommandRegistry reg;
	private final Command defaultCmd;

	public MetaCommand(String l, String[] a, Command[] cmds) {
		this.localepath = l;
		this.aliases = a;
		this.reg = new CommandRegistry();

		this.defaultCmd = cmds[0];
		for (Command c : cmds) {
			this.reg.register(c);
		}
	}

	@Override
	public LinkedHashMap<String, Argument> getArguments() {
		LinkedHashMap<String, Argument> args = new LinkedHashMap<>();
		args.put("command", new StringArgument(false));
		args.put("args", new FullStringArgument(false));
		return args;
	}

	@Override
	public CommandInfo getInfo() {
		return new CommandInfo(this.aliases, this.localepath);
	}

	@Override
	public void execute(ExecuteParams params) throws ExecuteException {
		StringArgument cmd = (StringArgument)params.arguments.get("command");
		FullStringArgument args = (FullStringArgument)params.arguments.get("args");

		Command realcmd = this.reg.getAliased(cmd.getValue());
		if (cmd.getValue().isEmpty()) {
			realcmd = this.defaultCmd;
		}

		if (realcmd != null) {
			params.log.debug("Executing subcommand " + realcmd.getInfo().aliases[0]);
			try {
				CommandParser parse = new CommandParser(args.getValue());
				LinkedHashMap<String, Argument> realargs = realcmd.getArguments();
				parse.parseArguments(realargs);

				realcmd.execute(new ExecuteParams(params.log, params.message, realargs, params.http, params.locale));
			} catch (ParseException e) {
				throw new ExecuteException("Failed to parse the arguments for the metacommand: " + e.getLocalizedMessage());
			}
		} else {
			throw new ExecuteException("No command named " + cmd.getValue() + " is in this metacommand.");
		}
	}
}
