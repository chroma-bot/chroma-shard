package rytone.chroma.shard.discord.commands.status;

import rytone.chroma.shard.discord.command.Command;
import rytone.chroma.shard.discord.command.MetaCommand;

public class StatusCommand {
	public static MetaCommand getCommand() {
		return new MetaCommand(
			"commands.status.meta",
			new String[] {
				"status",
				"stats"
			},
			new Command[] {
				new AllStatus(),
				new BackboneStatus(),
				new ShardStatus()
			}
		);
	}
}
