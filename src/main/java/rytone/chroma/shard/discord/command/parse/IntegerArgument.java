package rytone.chroma.shard.discord.command.parse;

public class IntegerArgument implements Argument<Integer> {
	private Integer parsed;
	
	public String parse(String in) throws ParseException {
		if (in.isEmpty()) {
			throw new ParseException("Argument requires content!");
		}
		String[] split = in.split("\\s+"); // TODO account for single quotes by not using regex?
		try {
			parsed = Integer.decode(split[0].trim());
		} catch (NumberFormatException e) {
			throw new ParseException("The supplied argument does not appear to be an integer.");
		}
		return in.substring(split[0].length(), in.length());
	}

	public Integer getValue() {
		return parsed;
	}
}
