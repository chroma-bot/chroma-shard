package rytone.chroma.shard.discord.command;

public class CommandInfo {
	public final String[] aliases;
	public final String localePath;

	public CommandInfo(String[] a, String l) {
		this.aliases = a;
		this.localePath = l;
	}
}
