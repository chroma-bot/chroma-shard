package rytone.chroma.shard.discord.command.parse;

public class PrefixUnmatchedException extends Exception {
	public PrefixUnmatchedException(String msg) {
		super(msg);
	}
}
