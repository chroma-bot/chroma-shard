package rytone.chroma.shard.discord.command.parse;

public class ParseException extends Exception {
	public ParseException(String msg) {
		super(msg);
	}
}
