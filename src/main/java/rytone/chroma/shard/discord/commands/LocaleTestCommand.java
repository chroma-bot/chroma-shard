package rytone.chroma.shard.discord.commands;

import java.util.LinkedHashMap;
import java.text.MessageFormat;

import rytone.chroma.shard.discord.command.Command;
import rytone.chroma.shard.discord.command.CommandInfo;
import rytone.chroma.shard.discord.command.ExecuteParams;
import rytone.chroma.shard.discord.command.parse.Argument;
import rytone.chroma.shard.discord.command.parse.StringArgument;
import rytone.chroma.shard.discord.command.parse.FlagArgument;

import rytone.chroma.shard.locale.LocaleException;

public class LocaleTestCommand implements Command {
	@Override
	public CommandInfo getInfo() {
		return new CommandInfo(
			new String[] {"localetest", "locale_test", "lct"},
			"commands.locale_test"
		);
	}

	@Override
	public LinkedHashMap<String, Argument> getArguments() {
		LinkedHashMap<String, Argument> args = new LinkedHashMap<>();
		args.put("locale", new StringArgument(true));
		args.put("path", new StringArgument(true));
		args.put("flags", new FlagArgument(new String[] {"abbrev"}));
		return args;
	}

	@Override
	public void execute(ExecuteParams params) {
		StringArgument locale = (StringArgument)params.arguments.get("locale");
		StringArgument path = (StringArgument)params.arguments.get("path");
		FlagArgument flags = (FlagArgument)params.arguments.get("flags");

		try {
		if (flags.getValue().get("abbrev")) {
			params.message.getChannel().sendMessage(params.locale.getByAbbrev(locale.getValue(), path.getValue())).queue();
		} else {
			params.message.getChannel().sendMessage(params.locale.get(locale.getValue(), path.getValue())).queue();
		}
		} catch (LocaleException e) {
			try {
				params.message.getChannel().sendMessage(MessageFormat.format(params.locale.get("english", "errors.locale.not_found"), locale.getValue())).queue();
			} catch (LocaleException ex) {
				// uuh?
			}
		}
	}
}
