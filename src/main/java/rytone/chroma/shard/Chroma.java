package rytone.chroma.shard;

import java.util.concurrent.ExecutionException;

import com.beust.jcommander.JCommander;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.AccountType;

import rytone.chroma.shard.config.CLIOptions;
import rytone.chroma.shard.config.Config;

import rytone.chroma.shard.backbone.Backbone;
import rytone.chroma.shard.backbone.ShardData;
import rytone.chroma.shard.discord.EventListener;

public class Chroma {
	public static void main(String args[]) throws InterruptedException, ExecutionException {
		CLIOptions opts = new CLIOptions();
		JCommander.newBuilder()
			.addObject(opts)
			.build()
			.parse(args);

		Logger log = LogManager.getLogger("Chroma");
		log.info("Starting...");
		log.debug("Starting init phase 1");

		log.trace("Loading config from {}", opts.configPath);
		Config cfg = Config.load(opts.configPath);

		log.debug("Establishing backbone connection...");
		Backbone backbone = new Backbone(cfg.backbone.ip, cfg.backbone.port);
		backbone.start();

		ShardData sDat = backbone.dataFuture.get();
		log.debug("Opening Discord session on shard {}/{}", sDat.shardID + 1, sDat.shardCount);
		
		try {
			JDABuilder jda = new JDABuilder(AccountType.BOT).setToken(cfg.shard.token).addEventListener(new EventListener(backbone, cfg));
			if (sDat.shardCount > 1) {
				jda.useSharding(sDat.shardID, sDat.shardCount);
			}
			jda.buildBlocking();
		} catch (Exception e) {
			log.error("Falied to open Discord session!");
			e.printStackTrace();
			backbone.shutdown();
		}
		
		backbone.join();
	}
}
