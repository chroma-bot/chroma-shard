package rytone.chroma.shard.config;

import com.beust.jcommander.Parameter;

public class CLIOptions {
	@Parameter(names = "--config", description = "Path to load configuration file from")
	public String configPath = "./config.toml";
}
