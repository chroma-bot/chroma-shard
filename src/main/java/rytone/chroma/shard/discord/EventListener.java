package rytone.chroma.shard.discord;

import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;

import java.io.InputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import java.text.MessageFormat;

import java.util.LinkedHashMap;
import java.util.concurrent.CompletableFuture;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import net.dv8tion.jda.core.entities.Icon;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.events.ReadyEvent;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

import okhttp3.OkHttpClient;

import rytone.chroma.shard.backbone.Backbone;
import rytone.chroma.shard.config.Config;

import rytone.chroma.shard.discord.command.Command;
import rytone.chroma.shard.discord.command.CommandRegistry;
import rytone.chroma.shard.discord.command.ExecuteException;
import rytone.chroma.shard.discord.command.ExecuteParams;
import rytone.chroma.shard.discord.command.parse.Argument;
import rytone.chroma.shard.discord.command.parse.CommandParser;
import rytone.chroma.shard.discord.command.parse.InvalidCommandException;
import rytone.chroma.shard.discord.command.parse.ParseException;
import rytone.chroma.shard.discord.command.parse.PrefixUnmatchedException;

import rytone.chroma.shard.discord.commands.*;
import rytone.chroma.shard.discord.commands.status.StatusCommand;

import rytone.chroma.shard.locale.LocaleProvider;
import rytone.chroma.shard.locale.LocaleException;

public class EventListener extends ListenerAdapter {
	private final Logger log;
	private final Logger commandLogger;

	private final Backbone backbone;
	private final Config config;

	private final CommandRegistry cmdReg;
	private final OkHttpClient cmdHttp;

	private final LocaleProvider locale;

	public EventListener(Backbone b, Config c) {
		this.log = LogManager.getLogger("EventListener");
		this.commandLogger = LogManager.getLogger("Command");
		this.backbone = b;
		this.config = c;
		this.locale = new LocaleProvider().load(c.paths.locale);

		this.cmdHttp = new OkHttpClient();

		this.cmdReg = new CommandRegistry();
		this.cmdReg.register(new EchoCommand());
		this.cmdReg.register(new TestCommand());
		this.cmdReg.register(new CatCommand());
		this.cmdReg.register(StatusCommand.getCommand());
		this.cmdReg.register(new LocaleTestCommand());
	}
	
	@Override
	public void onReady(ReadyEvent e) {
		this.log.debug("Successfully opened Discord sesion... starting init phase 2");

		this.log.debug("Updating avatar...");
		try {
			Path avPath = Paths.get(this.config.paths.assets, this.config.shard.avatar_path);
			InputStream avStream = Files.newInputStream(avPath);
			e.getJDA().getSelfUser().getManagerUpdatable().getAvatarField().setValue(Icon.from(avStream)).update().complete();
		} catch (IOException ex) {
			ex.printStackTrace();
			this.log.warn("Failed to set avatar due to an IOException... continuing anyway.");
		}

		RuntimeMXBean mxb = ManagementFactory.getRuntimeMXBean();
		long uptime = mxb.getUptime();
		log.info("Started in {} seconds. (since JVM start)", uptime / 1000.0);
	}

	private CompletableFuture executeCommand(Message m) {
		return CompletableFuture.supplyAsync(() -> {
			try {
				CommandParser parser = new CommandParser(m.getRawContent());
				parser.stripPrefix("//"); // TODO read from db
				String cmdname = parser.stripCommand();
				Command cmd = this.cmdReg.getAliased(cmdname);
				if (cmd != null) {
					this.log.debug("Executing command {}", cmdname);
					LinkedHashMap<String, Argument> args = cmd.getArguments();
					parser.parseArguments(args);
					cmd.execute(new ExecuteParams(this.commandLogger, m, args, this.cmdHttp, this.locale));
				}
			} catch (PrefixUnmatchedException ex) {
				// probably not trying to run a command
			} catch (InvalidCommandException ex) {
				// probably an accident
			} catch (ParseException ex) {
				this.log.debug("Failed to parse arguments: {}", ex.getLocalizedMessage());
				try {
					String estr = MessageFormat.format(this.locale.get("english", "errors.generic.arg_parse_fail"), ex.getLocalizedMessage());
					m.getChannel().sendMessage(estr).queue();
				} catch (LocaleException le) {
					// TODO send locale not found exception... or default to english
				}
			} catch (ExecuteException ex) {
				this.log.warn("Failed to execute command: {}", ex.getLocalizedMessage());
				try {
					String estr = MessageFormat.format(this.locale.get("english", "errors.generic.execute"), ex.getLocalizedMessage());
					m.getChannel().sendMessage(estr).queue();
				} catch (LocaleException e) {
					// TODO send locale not found exception... or default to english
				}
			}
			return null;
		});
	}

	@Override
	public void onMessageReceived(MessageReceivedEvent e) {
		Message m = e.getMessage();
		this.log.trace("Received message: {}#{}@{}: {}", m.getAuthor().getName(), m.getAuthor().getDiscriminator(), m.getGuild().getId(), m.getContent());

		if (m.getAuthor().equals(e.getJDA().getSelfUser())) return;

		this.executeCommand(m);
	}
}
