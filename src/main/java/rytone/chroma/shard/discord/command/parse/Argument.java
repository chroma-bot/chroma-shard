package rytone.chroma.shard.discord.command.parse;

public interface Argument<T> {
	public String parse(String in) throws ParseException;
	public T getValue();
}
