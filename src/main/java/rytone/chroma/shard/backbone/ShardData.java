package rytone.chroma.shard.backbone;

public class ShardData {
	public final int shardID;
	public final int shardCount;

	public ShardData(int i, int c) {
		this.shardID = i;
		this.shardCount = c;
	}
}
