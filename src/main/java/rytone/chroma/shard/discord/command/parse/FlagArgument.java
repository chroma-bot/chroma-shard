package rytone.chroma.shard.discord.command.parse;

import java.util.HashMap;

public class FlagArgument implements Argument<HashMap<String, Boolean>> {
	private HashMap<String, Boolean> parsed;
	
	public FlagArgument(String[] flags) {
		this.parsed = new HashMap<>();
		for (String f : flags) {
			this.parsed.put(f, false);
		}
	}

	public String parse(String in) throws ParseException {
		String[] split = in.split("\\s+");
		for (String s : split) {
			if (this.parsed.containsKey(s)) {
				this.parsed.put(s, true);
			}
		}
		return "";
	}

	public HashMap<String, Boolean> getValue() {
		return parsed;
	}
}
